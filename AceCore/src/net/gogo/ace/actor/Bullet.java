package net.gogo.ace.actor;

import java.util.ArrayList;

import net.gogo.ace.Asset;
import net.gogo.ace.actor.renderer.BasicActorRenderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

// TODO: bullet action may be one of 2 variation: seeking and straight
// TODO: bullet drawing may be one of 3 variation: GL, plain texture, animated texture
public class Bullet extends Actor {
	public Bullet(Texture texture, Vector2 position, Vector2 direction, ArrayList<Actor> targets) {
		mTexture = texture;

		setX(position.x);
		setY(position.y);

		mVelocity = new Vector2();
		mVelocity.set(direction).nor().mul(BULLET_SPEED);
		setRotation(mVelocity.angle() - 90);

		mTargets = targets;

		setWidth(1);
		setHeight(1);
	}

	@Override
	public void act(float delta) {
		super.act(delta);

		final float nextFrameX = getX() + mVelocity.x * delta;
		final float nextFrameY = getY() + mVelocity.y * delta;

		// check collision
		Vector2 bulletPosition = Vector2.tmp2.set(nextFrameX, nextFrameY);
		for (Actor actor: mTargets) {
			if (bulletPosition.dst(actor.getX(), actor.getY()) < 0.5f) {
				setVisible(false);
			}
		}
		setX(nextFrameX);
		setY(nextFrameY);
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		BasicActorRenderer.draw(this, mTexture, batch, parentAlpha);
	}

	public interface BulletSpawnListener {
		public void onBulletSpawn(Bullet bullet);
	}

	private ArrayList<Actor> mTargets;

	private Texture mTexture;

	private Vector2 mVelocity;

	private final static float BULLET_SPEED = 20.0f;
}
