package net.gogo.ace.actor;

import net.gogo.ace.Asset;
import net.gogo.ace.actor.renderer.BasicActorRenderer;
import net.gogo.ace.screen.GameScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class EnemyShip extends Actor implements TeamActor {
	public EnemyShip(Actor target) {
		mTexture = Asset.EnemyTexture;
		mTarget = target;

		setWidth(2.5f);
		setHeight(2.5f);
		setPosition(GameScreen.FRUSTUM_WIDTH + 5 * getWidth(), GameScreen.FRUSTUM_HEIGHT + 5 * getHeight());
	}

	@Override
	public int getTeamNumber() {
		return 2;
	}

	@Override
	public void act(float delta) {
		super.act(delta);

		// always face target
		if (mTarget != null) {
			Vector2.tmp.set(mTarget.getX() - getX(), mTarget.getY() - getY());
			setRotation(Vector2.tmp.angle() - 90);
		}
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		BasicActorRenderer.draw(this, mTexture, batch, parentAlpha);
	}

	private Texture mTexture;

	// lock on target!
	private Actor mTarget;
}
