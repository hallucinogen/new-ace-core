package net.gogo.ace.actor.renderer;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class BasicActorRenderer {
	public static void draw(Actor actor, Texture texture, SpriteBatch batch, float parentAlpha) {
		if (!actor.isVisible()) return;

		final float originX = actor.getWidth() / 2;
		final float originY = actor.getHeight() / 2;
		batch.draw(texture,
				actor.getX() - originX, actor.getY() - originY, // position
				originX, originY, // draw origin
				actor.getWidth(), actor.getHeight(), // width and height
				1.0f, 1.0f, // scaling
				actor.getRotation(), // rotation
				0, 0, texture.getWidth(), texture.getHeight(), // draw specific texture
				false, false // flip?
				);
	}
}
