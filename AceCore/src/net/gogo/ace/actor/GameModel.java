package net.gogo.ace.actor;

import java.util.ArrayList;

import net.gogo.ace.actor.Bullet.BulletSpawnListener;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;


public class GameModel extends Actor implements BulletSpawnListener {
	public GameModel(PlayerShip ship) {
		mShip 		= ship;
		mEnemies 	= new ArrayList<EnemyShip>();
		mWeapons 	= new ArrayList<Weapon>();
		mBullets 	= new ArrayList<Bullet>();
	}

	@Override
	public void act(float delta) {
		super.act(delta);

		for (Weapon weapon: mWeapons) {
			weapon.act(delta);
		}

		for (EnemyShip enemy: mEnemies) {
			enemy.act(delta);
		}

		for (Bullet bullet: mBullets) {
			bullet.act(delta);
		}

		mShip.act(delta);
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);

		for (Weapon weapon: mWeapons) {
			weapon.draw(batch, parentAlpha);
		}

		for (EnemyShip enemy: mEnemies) {
			enemy.draw(batch, parentAlpha);
		}

		for (Bullet bullet: mBullets) {
			bullet.draw(batch, parentAlpha);
		}

		mShip.draw(batch, parentAlpha);
	}

	@Override
	public void onBulletSpawn(Bullet bullet) {
		mBullets.add(bullet);
	}

	public void addEnemy(EnemyShip enemy) {
		mEnemies.add(enemy);
	}

	public void addWeapon(Weapon weapon) {
		mWeapons.add(weapon);
		weapon.setBulletSpawnListener(this);
	}

	// getters
	public PlayerShip getPlayer()				{	return mShip;		}
	public ArrayList<EnemyShip> getEnemies()	{	return mEnemies;	}
	public ArrayList<Weapon> getWeapons()		{	return mWeapons;	}
	public ArrayList<Bullet> getBullets()		{	return mBullets;	}

	private PlayerShip mShip;
	private ArrayList<Weapon> mWeapons;
	private ArrayList<EnemyShip> mEnemies;
	private ArrayList<Bullet> mBullets;
}
