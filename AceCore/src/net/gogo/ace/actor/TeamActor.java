package net.gogo.ace.actor;

public interface TeamActor {
	public int getTeamNumber();
	public float getX();
	public float getY();
}
