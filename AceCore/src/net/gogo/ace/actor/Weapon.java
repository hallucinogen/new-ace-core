package net.gogo.ace.actor;

import java.util.ArrayList;

import net.gogo.ace.Asset;
import net.gogo.ace.actor.renderer.BasicActorRenderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Weapon extends Actor {

	public Weapon(TeamActor owner, float distanceFromOwner, Texture texture, ArrayList<Actor> enemies) {
		// persist enemies for locking
		mEnemies 	= enemies;

		// persist data
		mOwner		= owner;
		mTexture 	= texture;

		mDistanceFromOwner 		= distanceFromOwner;
		mCurrentAngleFromOwner 	= 0;
		mInReloadTime			= 0;

		setWidth(1.5f);
		setHeight(1.5f);
	}

	@Override
	public void act(float delta) {
		super.act(delta);

		// position near owner
		mCurrentAngleFromOwner += ROTATION_SPEED * delta;
		mCurrentAngleFromOwner %= 360;

		Vector2 norm = Vector2.tmp;
		norm.nor();
		norm.setAngle(mCurrentAngleFromOwner);
		setX(mOwner.getX() + mDistanceFromOwner * norm.x);
		setY(mOwner.getY() + mDistanceFromOwner * norm.y);

		// always face target
		if (mTarget != null) {
			Vector2.tmp.set(mTarget.getX() - getX(), mTarget.getY() - getY());
			setRotation(Vector2.tmp.angle() - 90);
		} else {
			// find nearest target
			Vector2 temp = Vector2.tmp;
			temp.set(getX(), getY());
			float minDistance = Float.MAX_VALUE;
			for (Actor enemy : mEnemies) {
				float curDistance = temp.dst(enemy.getX(), enemy.getY());
				if (curDistance < minDistance) {
					mTarget = enemy;
				}
			}
		}

		mInReloadTime += delta;
		if (mInReloadTime > SHOOT_DELAY) {
			mInReloadTime -= SHOOT_DELAY;
			// shoot da bullet
			Vector2 weaponPosition = Vector2.tmp.set(getX(), getY());
			Vector2 direction = Vector2.tmp2.set(1, 1).nor();
			direction.setAngle(getRotation() + 90);
			Bullet bullet = new Bullet(Asset.PlayerTexture, weaponPosition, direction, mEnemies);

			if (mBulletSpawnListener != null) {
				mBulletSpawnListener.onBulletSpawn(bullet);
			}
		}
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		BasicActorRenderer.draw(this, mTexture, batch, parentAlpha);
	}

	public void setBulletSpawnListener(Bullet.BulletSpawnListener listener) {
		mBulletSpawnListener = listener;
	}

	// listen when bullet is spawned
	private Bullet.BulletSpawnListener mBulletSpawnListener;

	private ArrayList<Actor> mEnemies;
	private Actor mTarget;

	private Texture mTexture;

	// define weapon ownership
	private TeamActor mOwner;
	private float mDistanceFromOwner;

	// define state
	private float mCurrentAngleFromOwner;

	private float mInReloadTime;

	private final static float ROTATION_SPEED 	= 70;
	private final static float SHOOT_DELAY 		= 0.5f;
}
