package net.gogo.ace.actor;

import java.util.ArrayList;

import net.gogo.ace.Asset;
import net.gogo.ace.actor.renderer.BasicActorRenderer;
import net.gogo.ace.screen.GameScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class PlayerShip extends Actor implements TeamActor {
	public PlayerShip() {
		mTexture = Asset.PlayerTexture;

		setPosition(GameScreen.FRUSTUM_WIDTH, GameScreen.FRUSTUM_HEIGHT);
		setWidth(2.5f);
		setHeight(2.5f);

		mMoving 		= false;
		mVelocity 		= new Vector2();
		mDestination 	= new Vector2();

		move(0, 0);
	}

	@Override
	public int getTeamNumber() {
		return 1;
	}

	@Override
	public void act(float delta) {
		super.act(delta);

		if (mMoving) {
			Vector2 velocityToDestination = Vector2.tmp;
			Vector2 velocity = Vector2.tmp2;

			final float nextFrameX = getX() + mVelocity.x * delta;
			final float nextFrameY = getY() + mVelocity.y * delta;
			velocityToDestination.set(mDestination);
			velocityToDestination.sub(nextFrameX, nextFrameY);
			velocity.set(mVelocity);

			if (velocityToDestination.nor().epsilonEquals(velocity.nor(), 0.0001f)) {
				// still in right direction
				setX(nextFrameX);
				setY(nextFrameY);

				// when almost there, slow down
				float distance2 = mDestination.dst2(getX(), getY());
				if (distance2 < mVelocity.len2() * DECCELERATION) {
					mDeccelerate = true;
				}
				if (mDeccelerate) {
					mVelocity.set(mVelocity.x - mVelocity.x * DECCELERATION * delta, mVelocity.y - mVelocity.y * DECCELERATION * delta);
				}
			} else {
				setX(mDestination.x);
				setY(mDestination.y);
				mMoving = false;
			}
		}

		if (mTurning) {
			setRotation(getRotation() + (mRotationTarget - getRotation()) * delta * TURN_SPEED);
			if (Float.compare(mRotationTarget, getRotation()) == 0) {
				setRotation(mRotationTarget);
				mTurning = false;
			}
		}
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		BasicActorRenderer.draw(this, mTexture, batch, parentAlpha);
	}

	private void normalizeVelocity() {
		mVelocity.nor();
		mVelocity.mul(PLAYER_SPEED);
	}

	public void move(float dx, float dy) {
		mVelocity.set(dx, dy);
		normalizeVelocity();

		// -90 by math calculation :D
		mRotationTarget = mVelocity.angle() - 90;
	}

	public void moveTo(float x, float y) {
		float dx = x - getX();
		float dy = y - getY();
		mDestination.set(x, y);

		mMoving		= true;
		mTurning	= true;
		mDeccelerate = false;

		move(dx, dy);
	}
	
	private boolean mMoving, mTurning, mDeccelerate;

	private Vector2 mDestination;
	private float mRotationTarget;

	private Vector2 mVelocity;
	private Texture mTexture;

	private static final float PLAYER_SPEED 	= 20.0f;
	private static final float DECCELERATION 	= PLAYER_SPEED * 0.05f;
	private static final float TURN_SPEED 		= 5.0f;
}
