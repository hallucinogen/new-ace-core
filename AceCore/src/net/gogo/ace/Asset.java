package net.gogo.ace;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;

public class Asset {

	static {
		AssetManager = new AssetManager();
	}

	public static void prepare() {
		AssetManager.load("data/player.png", Texture.class);
		AssetManager.load("data/enemy.png", Texture.class);
	}

	public static void fetch() {
		PlayerTexture 	= AssetManager.get("data/player.png", Texture.class);
		EnemyTexture 	= AssetManager.get("data/enemy.png", Texture.class);
	}

	public static AssetManager AssetManager;

	public static Texture PlayerTexture;
	public static Texture EnemyTexture;
}
