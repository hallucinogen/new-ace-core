package net.gogo.ace;

import net.gogo.ace.screen.LoadingScreen;

import com.badlogic.gdx.Game;

public class MainEntry extends Game {
	@Override
	public void create() {
		Asset.prepare();
		setScreen(new LoadingScreen(this));
	}
}
