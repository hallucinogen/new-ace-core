package net.gogo.ace.screen;

import java.util.ArrayList;

import net.gogo.ace.Asset;
import net.gogo.ace.GameInputHandler;
import net.gogo.ace.actor.EnemyShip;
import net.gogo.ace.actor.GameModel;
import net.gogo.ace.actor.PlayerShip;
import net.gogo.ace.actor.Weapon;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

// control the model and stage, tie them together
public class GameScreen implements Screen {

	public GameScreen(Game game) {
		mGame = game;
	}

	@Override
	public void show() {

		// prepare model
		final PlayerShip player = new PlayerShip();
		mGameModel = new GameModel(player);
		mGameModel.addEnemy(new EnemyShip(player));

		ArrayList<Actor> weaponTestTargets = new ArrayList<Actor>();
		for (Actor actor: mGameModel.getEnemies()) {
			weaponTestTargets.add(actor);
		}
		Weapon weaponTest = new Weapon(player, 2.0f, Asset.EnemyTexture, weaponTestTargets);
		mGameModel.addWeapon(weaponTest);
	
		// set model as stage main actor
		mStage = new Stage();
		mStage.addActor(mGameModel);

		// set input listener
		mStage.addListener(new GameInputHandler(mGameModel.getPlayer()));

		// combine inputs
		InputMultiplexer im = new InputMultiplexer(mStage);
		Gdx.input.setInputProcessor(im);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor( 0f, 0f, 0f, 1f );
        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );

		final Camera camera = mStage.getCamera();
		final PlayerShip player = mGameModel.getPlayer();

		camera.position.y = player.getY() + player.getHeight() / 2;
		camera.position.x = player.getX() + player.getWidth() / 2;

		mStage.act(delta);
		mStage.draw();
	}

	@Override
	public void resize(int width, int height) {
		mStage.setViewport(FRUSTUM_WIDTH, FRUSTUM_HEIGHT, true);
	}

	@Override
	public void dispose() {
		mStage.dispose();
	}

	// main stage
	private Stage mStage;

	// mGame is for screen transition
	private Game mGame;

	// mGameModel is for game model and main logic
	private GameModel mGameModel;


	public static final float FRUSTUM_HEIGHT = 25;
	public static final float FRUSTUM_WIDTH = 15;
}
