package net.gogo.ace;

import net.gogo.ace.actor.PlayerShip;
import net.gogo.ace.screen.GameScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class GameInputHandler extends InputListener {

	public GameInputHandler(PlayerShip ship) {
		mShip = ship;
	}

	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		switch (keycode) {
		case Keys.W:
			mShip.move(0, 1);
			break;
		case Keys.S:
			mShip.move(0, -1);
			break;
		case Keys.A:
			mShip.move(-1, 0);
			break;
		case Keys.D:
			mShip.move(1, 0);
			break;
		}

		return false;
	}

	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer,
			int button) {

		mShip.moveTo(x, y);
		return true;
	}

	@Override
	public void touchDragged(InputEvent event, float x, float y, int pointer) {
		//mShip.move(x - mShip.getX(), y - mShip.getY());
	}
	
	private PlayerShip mShip;
}
