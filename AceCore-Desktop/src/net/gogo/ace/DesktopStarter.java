package net.gogo.ace;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopStarter {
	public static void main(String[] args) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title 	= "Ace Core";
		config.useGL20 	= true;
		config.width 	= 800;
		config.height 	= 480;

		new LwjglApplication(new MainEntry(), config);
	}
}
